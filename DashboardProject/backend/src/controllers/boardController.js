const {JoiBoardSchema, BoardSchema} = require("../models/Board");
const mongoose = require("mongoose");
const {createBoard} = require("../services/boardService");


const addBoard = async (req, res) => {
    try {
        const {name, description} = req.body;

        await JoiBoardSchema.validateAsync({name, description})

        await createBoard({created_by: req.user._id, name, description})
            .then(() => {
                res.status(200).json({message: 'Board created successfully'});
            })
            .catch((e) => {
                res.status(500).json({message: e});
            });
    } catch {
        return res.status(400).json({message: 'Invalid input values'});
    }
}
const deleteBoardByID = async (req, res) => {
    try {
        const boardID = req.params.id;
        if (!boardID) {
            return res.status(400).send({message: 'Incorrect ID'});
        }
        const board = await BoardSchema.findOne({created_by: req.user._id, _id: boardID})
        if (!board) {
            return res.status(400).send({message: 'Board doesn`t exist'});
        }
        await board.remove()
            .then(() => {
                res.status(200).json({
                    message: 'Board deleted successfully',
                });
            })
            .catch(() => {
                res.status(500).send({message: 'Internal server error'});
            })

    } catch (e) {
        return res.status(400).json({message: e.message});
    }
}
const editBoardByID = async (req, res) => {
    try {
        const boardID = req.params.id;
        const name = req.body.name;
        if (!boardID) {
            return res.status(400).send({message: 'Incorrect ID'});
        }
        const board = await BoardSchema.findOne({created_by: req.user._id, _id: boardID})
        if (!board) {
            return res.status(400).send({message: 'Board doesn`t exist'});
        }
        await BoardSchema.findOneAndUpdate({board}, {name}, {new: true})
            .then((data) => {
                res.status(200).json({
                   message:'Board updated successfully',
                    data
                });
            })
            .catch(() => {
                res.status(500).send({message: 'Internal server error'});
            })

    } catch (e) {
        return res.status(400).json({message: e.message});
    }
}
const getAllBoards = async (req,res)=>{
    try {
        return BoardSchema.find({ created_by: req.user._id })
            .then((boards) => {
                res.status(200).json({ boards });
            })
            .catch(() => res.status(500).json({ message: 'Internal server error' }));
    } catch (e) {
        return res.status(400).json({ message: 'Bad request' });
    }
}
module.exports = {
    addBoard,
    deleteBoardByID,
    editBoardByID,
    getAllBoards
}
