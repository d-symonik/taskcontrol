const {JoiTaskSchema, TaskSchema} = require("../models/Task");
const {createTask} = require("../services/taskService");
const {BoardSchema} = require("../models/Board");


const addTask = async (req, res) => {
    try {
        const {name, status} = req.body;

        const board_id = req.params.board_id;
        await JoiTaskSchema.validateAsync({name, status});

        await createTask({created_by: req.user._id, board_id, name, status})
            .then((data) => {
                res.status(200).json({message: 'Task created successfully', data});
            })
            .catch((e) => {
                res.status(500).json({message: e});
            });
    } catch (e) {
        return res.status(400).json({message: e.message});
    }
}

const editTaskByID = async (req, res) => {
    try {
        const taskID = req.params.id;
        const name = req.body.name;
        if (!taskID) {
            return res.status(400).send({message: 'Incorrect ID'});
        }
        const task = await TaskSchema.findOne({created_by: req.user._id, _id: taskID})
        if (!task) {
            return res.status(400).send({message: 'Task doesn`t exist'});
        }
        await TaskSchema.findOneAndUpdate({task}, {name}, {new: true})
            .then((data) => {
                res.status(200).json({
                    message: 'Task updated successfully',
                    task: data
                });
            })
            .catch(() => {
                res.status(500).send({message: 'Internal server error'});
            })

    } catch (e) {
        res.status(400).json({message: e.message});
    }
}

const deleteTaskByID = async (req, res) => {
    try {
        const taskID = req.params.id;
        if (!taskID) {
            return res.status(400).send({message: 'Incorrect ID'});
        }
        const task = await TaskSchema.findOne({created_by: req.user._id, _id: taskID})
        if (!task) {
            return res.status(400).send({message: 'Task doesn`t exist'});
        }
        await task.remove()
            .then((data) => {
                res.status(200).json({
                    message: 'Board deleted successfully',
                    task: data
                });
            })
            .catch(() => {
                res.status(500).send({message: 'Internal server error'});
            })

    } catch (e) {
        return res.status(400).json({message: e.message});
    }
}

const addComment = async (req, res) => {
    try {
        const taskID = req.params.id;
        const message = req.body.message;
        if (!message) {
            return res.status(400).send({message: 'Incorrect comment'});
        }
        if (!taskID) {
            return res.status(400).send({message: 'Incorrect ID'});
        }
        const task = await TaskSchema.findOne({created_by: req.user._id, _id: taskID})
        if (!task) {
            return res.status(400).send({message: 'Task doesn`t exist'});
        }
        await TaskSchema.findOneAndUpdate({task}, {$push: {comments: {message}}}, {new: true})
            .then((data) => {
                res.status(200).json({
                    message: 'Task updated successfully',
                    task: data
                });
            })
            .catch((e) => {
                res.status(500).send({message: e.message});
            })

    } catch (e) {
        res.status(400).json({message: e.message});
    }
}

const deleteComment = async (req, res) => {
    try {

        const {id, message_id} = req.params;


        if (!id) {
            return res.status(400).send({message: 'Incorrect ID'});
        }
        const task = await TaskSchema.findOne({created_by: req.user._id, _id: id})
        if (!task) {
            return res.status(400).send({message: 'Task doesn`t exist'});
        }
        await TaskSchema.findOneAndUpdate({task}, {$pull: {comments: {_id: message_id}}}, {new: true})
            .then((data) => {
                res.status(200).json({
                    message: 'Task updated successfully',
                    task: data
                });
            })
            .catch((e) => {
                res.status(500).send({message: e.message});
            })

    } catch (e) {
        res.status(400).json({message: e.message});
    }
}

const changeStatus = async (req, res) => {
    try {
        const taskID = req.params.id;
        const status = req.body.status;
        await JoiTaskSchema.validateAsync({name: '-', status})
        if (!taskID) {
            return res.status(400).send({message: 'Incorrect ID'});
        }
        const task = await TaskSchema.findOne({created_by: req.user._id, _id: taskID})
        if (!task) {
            return res.status(400).send({message: 'Task doesn`t exist'});
        }
        await TaskSchema.findOneAndUpdate({task}, {status}, {new: true})
            .then((data) => {
                res.status(200).json({
                    message: 'Task updated successfully',
                    task: data
                });
            })
            .catch(() => {
                return res.status(500).send({message: 'Internal server error'});
            })
    } catch (e) {
        res.status(400).json({message: e.message});
    }
}
const getAllTasks = async (req, res) => {
    try {
        const board_id = req.params.board_id;

        return TaskSchema.find({board_id})
            .then((tasks) => {
                res.status(200).json({tasks});
            })
            .catch(() => res.status(500).json({message: 'Internal server error'}));
    } catch (e) {
        return res.status(400).json({message: 'Bad request'});
    }
}
module.exports = {
    addTask,
    editTaskByID,
    deleteTaskByID,
    addComment,
    deleteComment,
    changeStatus,
    getAllTasks
}

