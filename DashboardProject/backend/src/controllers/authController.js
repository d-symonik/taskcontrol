const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const {JoiUserSchema, UserSchema} = require("../models/User");
const {createUser} = require("../services/userService");

dotenv.config();

const registerUser = async (req, res) => {
    try {
        const {username, email, password} = req.body;
        await JoiUserSchema.validateAsync(req.body);
        const candidate = await UserSchema.findOne({email});
        if (candidate) {
            return res.status(400).json({message: 'This user already exist'});
        }
        await createUser({username, email, password}).then((data) => {
            res.status(200).json({message: 'Profile created successfully',data});
        })
            .catch(() => {
                res.status(500).json({message: 'Server error'});
            });
    } catch (err) {
        return res.status(400).json({message: 'Input wrong form data'});
    }
}

const loginUser = async (req, res) => {
    try {
        const {email, password} = req.body;
        const user = await UserSchema.findOne({email});

        if (user && await bcryptjs.compare(String(password), String(user.password))) {
            const payload = {
                userId: user._id, username: user.username, email: user.email
            };
            const jwtToken = jwt.sign(payload, process.env.SECRET_KEY/*, {expiresIn: '1h'}*/);
            if (!jwtToken) {
                return res.status(500).json({message: 'Server error'});
            }
            return res.status(200).send({jwt_token: jwtToken});
        }

        return res.status(400).json({message: 'Not authorized'});
    } catch (e) {
        return res.status(400).json({message: 'Something went wrong'});
    }


}

module.exports = {
    registerUser,
    loginUser
}
