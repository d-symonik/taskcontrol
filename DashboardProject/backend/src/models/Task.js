const mongoose = require('mongoose');
const Joi = require('joi')

const JoiTaskSchema = Joi.object({

    name: Joi
        .string()
        .min(1)
        .required(),

    status: Joi
        .string()
        .valid('TODO', 'IN PROGRESS', 'DONE', 'ARCHIVED'),
    comment:Joi.array().items({
        message: Joi.string()
    })
});

const TaskSchema = new mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    board_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Board',
        required:true
    },
    name: {
        type: String,
        require: true,
    },

    status: {
        type: String,
        enum: ['TODO', 'IN PROGRESS', 'DONE', 'ARCHIVED'],

    },
    comments: [{
        message: {type: String},
    }]
}, {timestamps: {createdAt: 'created_date', updatedAt: false}, versionKey: false});

module.exports = {
    TaskSchema: mongoose.model('Tasks', TaskSchema),
    JoiTaskSchema,
};
