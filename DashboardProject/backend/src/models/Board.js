const mongoose = require('mongoose');
const Joi = require('joi')

const JoiBoardSchema = Joi.object({

    name: Joi
        .string()
        .min(1)
        .max(50)
        .required(),

    description: Joi
        .string()

});

const BoardSchema = new mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    name: {
        type: String,
        require: true,
    },
    description: {
        type: String,
    },
}, {timestamps: {createdAt: 'created_date', updatedAt: false}, versionKey: false});

module.exports = {
    BoardSchema: mongoose.model('Boards', BoardSchema),
    JoiBoardSchema,
};
