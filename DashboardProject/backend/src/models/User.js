const mongoose = require('mongoose');
const Joi = require('joi');

const JoiUserSchema = Joi.object({
  username: Joi
    .string()
    .alphanum()
    .min(4)
    .max(30)
    .required(),
  email: Joi
    .string()
    .email()
    .required(),

  password: Joi
    .string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/),
});

const UserSchema = mongoose.Schema({
  username: {
    type: String,
    require: true,
    unique: true,
  },
  email: {
    type: String,
    require: true,
  },
  password: {
    type: String,
    require: true,
  },
}, { versionKey: false });

module.exports = {
  UserSchema: mongoose.model('Users', UserSchema),
  JoiUserSchema,
};
