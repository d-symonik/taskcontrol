const bcryptjs = require('bcryptjs');
const { UserSchema } = require('../models/User');

const createUser = async ({ username,email, password}) => {
    const user = new UserSchema({
        username,
        email,
        password: await bcryptjs.hash(password, 10),

    });

    return user.save();
};

module.exports = {
    createUser,
};
