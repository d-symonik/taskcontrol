const {BoardSchema} = require("../models/Board");

const createBoard = async ({ created_by, name, description}) => {
    const board = new BoardSchema({
        created_by,
        name,
        description
    });

    return board.save();
};

module.exports = {
    createBoard,
};
