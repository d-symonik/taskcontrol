const {TaskSchema} = require("../models/Task");

const createTask = async ({created_by, board_id, name, status}) => {
    const task = new TaskSchema({
        created_by,
        board_id,
        name,
        status
    });

    return task.save();
};

module.exports = {
    createTask,
};
