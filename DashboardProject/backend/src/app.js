const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const { authRouter } = require('./routers/authRouter');
const {boardRouter} = require("./routers/boardRouter");

require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 3000;
app.use(express.json());
app.use(cors())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
  next();
});


app.use('/api/auth', authRouter);
app.use('/api/boards', boardRouter);


  try {
    mongoose.connect(process.env.DB_CON);
    app.listen(PORT, () => {
      console.log(`Server is working on port ${PORT}`);
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }



