const express = require('express');
const {registerUser, loginUser} = require("../controllers/authController");
const {authMiddleware} = require("../middlewares/authMiddleware");
const router = express.Router();

router.post('/register',registerUser);
router.post('/login',loginUser);
// router.get('/user',authMiddleware,getInfo)
module.exports = {
    authRouter: router,
};
