const express = require('express');
const {authMiddleware} = require("../middlewares/authMiddleware");
const {addBoard, deleteBoardByID, editBoardByID, getAllBoards} = require("../controllers/boardController");
const {taskRouter} = require("./taskRouter");
const router = express.Router();

// add new board
router.post('/create', authMiddleware,addBoard);
// delete board
router.delete('/delete/:id', authMiddleware, deleteBoardByID);
// edit board
router.patch('/update/:id', authMiddleware, editBoardByID);
// get all boards
router.get('/',authMiddleware, getAllBoards);

//taskRouter
router.use('/tasks',taskRouter);

module.exports = {
    boardRouter: router
}
