const express = require('express');
const {authMiddleware} = require("../middlewares/authMiddleware");
const {addTask, editTaskByID, deleteTaskByID, addComment, deleteComment, changeStatus, getAllTasks} = require("../controllers/taskController");
const router = express.Router();

// get all task for display in board
router.get('/:board_id',authMiddleware,getAllTasks);

//add new task

router.post('/:board_id',authMiddleware,addTask)

//update the task
router.patch('/:board_id/:id',authMiddleware,editTaskByID)

// remove task by id
router.delete('/:board_id/:id',authMiddleware,deleteTaskByID);

//add new comment in the task
router.patch('/:board_id/:id/comments',authMiddleware,addComment);

//remove comment in the task
router.delete('/:board_id/:id/comments/:message_id',authMiddleware,deleteComment);

//change the status of the task

router.patch('/:board_id/:id/status',authMiddleware,changeStatus)
module.exports = {
    taskRouter:router
}
