import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListOfTasksComponent} from "./list-of-tasks/list-of-tasks.component";
import {DashboardComponent} from "./dashboard.component";
import {ListOfBoardsComponent} from "./list-of-boards/list-of-boards.component";

const routes: Routes = [

  {
    path: '', component: DashboardComponent,
    children: [
      {path:'boards',component:ListOfBoardsComponent},
      {path: 'tasks/:board_id', component: ListOfTasksComponent},

    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
