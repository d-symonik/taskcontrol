import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ListOfBoardsComponent } from './list-of-boards/list-of-boards.component';
import { BoardComponent } from './list-of-boards/board/board.component';
import { ListOfTasksComponent } from './list-of-tasks/list-of-tasks.component';
import { TaskComponent } from './list-of-tasks/task/task.component';
import { DashboardComponent } from './dashboard.component';
import {CoreModule} from "../../core/core.module";
import {HeaderComponent} from "./header/header.component";
import { BreadCrumbsComponent } from './bread-crumbs/bread-crumbs.component';


@NgModule({
  declarations: [
    ListOfBoardsComponent,
          BoardComponent,
          ListOfTasksComponent,
          TaskComponent,
          DashboardComponent,
    HeaderComponent,
    BreadCrumbsComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CoreModule
  ]
})
export class DashboardModule { }
