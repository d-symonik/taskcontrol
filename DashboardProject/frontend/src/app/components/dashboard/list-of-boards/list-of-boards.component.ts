import {Component, Input, OnInit, Output} from '@angular/core';
import {Board} from "../../../shared/models/Board";
import {DataService} from "../../../core/services/data.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-list-of-boards',
  templateUrl: './list-of-boards.component.html',
  styleUrls: ['./list-of-boards.component.scss']
})
export class ListOfBoardsComponent implements OnInit {
   listOfTheBoard!: Board[]

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getAllBoards().subscribe(res => {
      this.listOfTheBoard = res.boards
      console.log(this.listOfTheBoard)
    })
  }
ngOnChanges():void{
  this.dataService.getAllBoards().subscribe(res=> this.listOfTheBoard=[...res.boards])
}
}
