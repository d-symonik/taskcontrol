import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService} from "../../../../core/services/data.service";
import {Board} from "../../../../shared/models/Board";

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  @Input() nameOfBoard:String = '';
  @Input() dateOfTheCreation = '';
  @Input() board_id='';
  @Input() ListOfBoard:Board[] = [];
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  }

  deleteBoard(board_id:string) {
    this.dataService.deleteBoard(board_id).subscribe(data=>alert(data.message));



  }
}
