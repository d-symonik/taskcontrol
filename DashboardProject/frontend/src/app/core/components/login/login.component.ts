import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {map, Observable, pipe} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  jwt_token$!: Object

  constructor(private router:Router, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)])
    })
  }


  loginUser() {
    this.authService.loginUser(this.loginForm.value).subscribe({
      next:(data) => {

        this.authService.setToken(data.jwt_token)
        console.log(this.authService.getToken())
        this.router.navigate(['dashboard'])
      },
      error:(err)=>alert('Wrong email or password. Please try again')
    })
  }
}


