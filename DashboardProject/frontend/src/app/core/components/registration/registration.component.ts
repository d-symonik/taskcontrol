import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../shared/models/User";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  signUpForm!: FormGroup;

  constructor(private authService:AuthService) {
  }

  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      'username': new FormControl('', [Validators.required, Validators.min(8), Validators.max(20)]),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)])
    })
  }

  submitSignUp() {

  }

  postNewUser(user:User) {
    return this.authService.sendNewUser(user).subscribe((data)=>console.log(data));
  }

}
