import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './components/login/login.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ReactiveFormsModule} from "@angular/forms";
import {RouterLinkWithHref} from "@angular/router";
import {RegistrationComponent} from "./components/registration/registration.component";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    NotFoundComponent,
    LoginComponent,
    RegistrationComponent
  ],
  exports: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterLinkWithHref,
    HttpClientModule
  ]
})
export class CoreModule {
}
