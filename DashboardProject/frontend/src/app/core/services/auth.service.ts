import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../../shared/models/User";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, map, pipe} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private http: HttpClient) {
  }

  setToken(token: string) {
     localStorage.setItem('token', token)
  }

  getToken() {
    return localStorage.getItem('token')
  }

  loginUser(user: User) {
   return this.http.post<{ jwt_token: string }>('http://localhost:3000/api/auth/login', user);
  }
  isLoggedIn(){
    return this.getToken() !==null
  }
  sendNewUser(user: User) {
    return this.http.post('http://localhost:3000/api/auth/register', user)
  }

}
