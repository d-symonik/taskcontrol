import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {Board} from "../../shared/models/Board";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
  }
  deleteBoard(board_id:string){
    return this.http.delete<{message:string}>(`http://localhost:3000/api/boards/delete/${board_id}`)
  }
  getAllBoards() {
    return this.http.get<{boards:Board[]}>('http://localhost:3000/api/boards')
  }
}
