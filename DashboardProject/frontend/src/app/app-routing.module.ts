import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./core/components/login/login.component";
import {NotFoundComponent} from "./core/components/not-found/not-found.component";
import {RegistrationComponent} from "./core/components/registration/registration.component";
import {ListOfBoardsComponent} from "./components/dashboard/list-of-boards/list-of-boards.component";

const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'registration',component:RegistrationComponent},
  {path:'dashboard',
    loadChildren:()=>import('./components/dashboard/dashboard.module').then((m)=>m.DashboardModule)
  },
  {path:'', redirectTo:'/login',pathMatch:'full'},
  {path:'**',component:NotFoundComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
