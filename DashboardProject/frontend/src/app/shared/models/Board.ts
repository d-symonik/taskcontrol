export interface Board{
  _id:string;
  created_by:string;
  name:string;
  description:string
  created_date:string,
}
